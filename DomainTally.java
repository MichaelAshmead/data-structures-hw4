//Michael Ashmead (mashmea1)

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * DomainTally.
 */
public class DomainTally {
    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        int k = Integer.parseInt(args[0]);
        if (k <= 0) {
            throw new IllegalArgumentException();
        }
        int urlCount = 0;
        DomainTally dt = new DomainTally();
        MyJHUHashMap<String> m = dt.new MyJHUHashMap<String>(k);
        if (args.length == 2) { //file name given
            //process lines in file
            String filename = args[1];
            System.out.println("Processing the file " + filename + "...");
            File file = new File(filename);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            while (line != null && !line.equals("quit")) {
                line = line.toLowerCase();
                int count = line.length() - line.replace(".", "").length();
                if (line.equals("report")) {
                    System.out.println("REPORT: After collecting " + urlCount + " URLs total, the top " + k + " domains are:");
                    m.printKeysInOrder();
                }
                else if (count > 1) {
                    urlCount++;
                    int positionOfExt;
                    if (line.contains(".com")) {
                        positionOfExt = line.indexOf(".com");
                    }
                    else if (line.contains(".edu")) {
                        positionOfExt = line.indexOf(".edu");
                    }
                    else if (line.contains(".gov")) {
                        positionOfExt = line.indexOf(".gov");
                    }
                    else if (line.contains(".net")) {
                        positionOfExt = line.indexOf(".net");
                    }
                    else {
                        positionOfExt = line.indexOf(".org");
                    }
                    line = line.substring(0, positionOfExt + 2 + 2);
                    line = line.substring(line.substring(0,line.lastIndexOf(".") - 1).lastIndexOf(".") + 1);
                    m.put(line);
                }
                else {
                    urlCount++;
                    m.put(line);
                }
                line = br.readLine();
            }
            String s = "File processing complete, beginning interactive stage.";
            System.out.println(s);
            br.close();
        }
        BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while ((line = keyboard.readLine()).equals("quit") == false) {
            line = line.toLowerCase();
            int count = line.length() - line.replace(".", "").length();
            if (line.equals("report")) {
                System.out.println("REPORT: After collecting " + urlCount + " URLs total, the top " + k + " domains are:");
                m.printKeysInOrder();
            } else if (count > 1) {
                urlCount++;
                int positionOfExt;
                if (line.contains(".com")) {
                    positionOfExt = line.indexOf(".com");
                } else if (line.contains(".edu")) {
                    positionOfExt = line.indexOf(".edu");
                } else if (line.contains(".gov")) {
                    positionOfExt = line.indexOf(".gov");
                } else if (line.contains(".net")) {
                    positionOfExt = line.indexOf(".net");
                } else {
                    positionOfExt = line.indexOf(".org");
                }
                line = line.substring(0, positionOfExt + 2 + 2);
                line = line.substring(line.indexOf(".") + 1);
                m.put(line);
            } else {
                urlCount++;
                m.put(line);
            }
        }
        keyboard.close();
    }
    
    /**
     * @param <K> keys
     */
    public class MyJHUHashMap<K> {
        
        /**
         * maxCapacity.
         */
        private int maxCapacity;
        
        /**
         * load.
         */
        private double load;
        
        /**
         * nodeCount.
         */
        private int nodeCount;
        
        /**
         * domainsWanted.
         */
        private int domainsWanted;
        
        /**
         * head.
         */
        private Node head;
        
        /**
         * map.
         */
        private Node[] map;
        
        /**
         * Node class.
         */
        private class Node {

            /**
             * Key of node.
             */
            private K key = null;
            
            /**
             * count.
             */
            private int count = 1;
            
            /**
             * before.
             */
            private Node before = null;
            
            /**
             * after.
             */
            private Node after = null;
        }

        /**
         * @param k domainsWanted
         */
        @SuppressWarnings("unchecked")
        public MyJHUHashMap(int k) {
            final int eleven = 11;
            final double finalLoad = .5;
            this.maxCapacity = eleven;
            this.load = finalLoad;
            this.nodeCount = 0;
            this.head = new Node();
            this.head.before = this.head;
            this.head.after = this.head;
            this.domainsWanted = k;
            this.map = (Node[]) new MyJHUHashMap.Node[this.maxCapacity];
        }

        /**
         * @param k - key to be inserted
         */
        public void put(K k) {
            int firstHash = Math.abs(k.hashCode() % this.maxCapacity);
            int hash = firstHash;
            
            this.rehashIfNeeded();
            
            Node tempNode = new Node();
            tempNode.key = k;
            
            if (this.containsKey(k)) { //key already exists
                while (this.map[hash] == null || !this.map[hash].key.equals(k)) {
                    hash = ((hash + 1) % (this.maxCapacity));
                }
                
                //node1, tempNode, node2, node3 <----- nodes under consideration
                tempNode = this.map[hash];
                tempNode.count++;
                
                while (tempNode.count > tempNode.after.count && tempNode.after.key != null) {
                    //swapping nodes
                    Node node1 = tempNode.before;
                    Node node2 = tempNode.after;
                    Node node3 = tempNode.after.after;
                    
                    //remove tempNode
                    node1.after = node2;
                    node2.before = node1;
                    
                    //place tempNode in between node2 and node 3
                    node2.after = tempNode;
                    node3.before = tempNode;
                    
                    //fix tempNode's pointers
                    tempNode.before = node2;
                    tempNode.after = node3;
                }
            } else { //key not already in map
                while (this.map[hash] != null) {
                    hash = ((hash + 1) % (this.maxCapacity));
                }
                
                //inserting node to front of chain
                tempNode.before = this.head;
                tempNode.after = this.head.after;
                tempNode.after.before = tempNode;
                if (this.nodeCount == 0) {
                    this.head.before = tempNode;
                }
                this.head.after = tempNode;
                
                this.nodeCount++;
                this.map[hash] = tempNode;
            }
        }

        /**
         * rehashIfNeeded().
         */
        private void rehashIfNeeded() {
            if (this.nodeCount > (this.load * this.maxCapacity)) {
                this.resizeAndRehash();
            }
        }
        
        /**
         * resizeAndRehash().
         */
        @SuppressWarnings("unchecked")
        private void resizeAndRehash() {
            int twice = this.maxCapacity * 2;
            Node[] newMap = (Node[]) new MyJHUHashMap.Node[twice];

            for (int x = 0; x < this.maxCapacity; x++) {
                if (this.map[x] != null) {
                    Node tempNode = this.map[x];
                    int hash = Math.abs((tempNode.key.hashCode() % (twice)));
                    
                    while (newMap[hash] != null) {
                        hash = ((hash + 1) % (this.maxCapacity * 2));
                    }
                    
                    newMap[hash] = tempNode;
                }
            }
            this.maxCapacity = this.maxCapacity * 2;
            this.map = newMap;
        }

        /**
         * @param k key
         * @return boolean
         */
        public boolean containsKey(Object k) {
            int firstHash = Math.abs((k.hashCode() % (this.maxCapacity)));
            int hash = firstHash;
            
            while (this.map[hash] == null || !this.map[hash].key.equals(k)) {
                hash = ((hash + 1) % (this.maxCapacity));
                if (hash == firstHash) {
                    return false;
                }
            }
            return true;
        }
        
        /**
         * printKeysInOrder().
         */
        public void printKeysInOrder() {
            ArrayList<Node> nodes = new ArrayList<Node>();
            Node current = this.head;
            for (int x = 0; x < this.domainsWanted; x++) {
                if (current.before != null) {
                    nodes.add(current.before);
                }
                current = current.before;
                
                if (current.before.key == null) {
                    //break out of loop if looping back to head
                    break;
                }
            }
            for (int x = 0; x < nodes.size(); x++) {
                System.out.println(nodes.get(x).key);
            }
        }
    }
}
