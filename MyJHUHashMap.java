//Michael Ashmead (mashmea1)

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @param <K> keys
 * @param <V> values
 */
public class MyJHUHashMap<K, V> implements JHUHashMap<K, V> {

    /**
     * maxCapacity.
     */
    private static int maxCapacity;
    
    /**
     * load.
     */
    private static double load;
    
    /**
     * nodeCount.
     */
    private int nodeCount;
    
    /**
     * map.
     */
    private Node[] map;
    
    /**
     * Node class.
     */
    private class Node {

        /**
         * Key of node.
         */
        private K key = null;

        /**
         * Value of node.
         */
        private V value = null;
    }
    
    /**
     * Constructor.
     */
    @SuppressWarnings("unchecked")
    public MyJHUHashMap() {
        final int eleven = 11;
        final double finalLoad = .5;
        maxCapacity = eleven;
        load = finalLoad;
        this.nodeCount = 0;
        this.map = (Node[]) new MyJHUHashMap.Node[maxCapacity];
    }

    /**
     * @param c set to maxCapacity
     * @param l set to load
     */
    @SuppressWarnings("unchecked")
    public MyJHUHashMap(int c, double l) {
        if (c < 0 || l < 0) {
            throw new IllegalArgumentException();
        } else {
            maxCapacity = c;
            load = l;
            this.nodeCount = 0;
            this.map = (Node[]) new MyJHUHashMap.Node[maxCapacity];
        }
    }

    @Override
    public V put(K k, V v) {
        int firstHash = Math.abs(k.hashCode() % maxCapacity);
        int hash = firstHash;
        
        this.rehashIfNeeded();
        
        Node tempNode = new Node();
        tempNode.key = k;
        tempNode.value = v;
        
        if (this.containsKey(k)) { //key already exists
            while (this.map[hash] == null || !this.map[hash].key.equals(k)) {
                hash = ((hash + 1) % (maxCapacity));
            }
            V oldValue = this.map[hash].value;
            this.map[hash].value = v;
            return oldValue; 
        } else { //key not already in map
            while (this.map[hash] != null) {
                hash = ((hash + 1) % (maxCapacity));
                if (hash == firstHash) { //no more empty buckets
                    return null;
                }
            }
            
            this.nodeCount++;
            this.map[hash] = tempNode;
            return null;
        }
    }

    /**
     * rehashIfNeeded().
     */
    private void rehashIfNeeded() {
        if (this.nodeCount > (load * maxCapacity)) {
            this.resizeAndRehash();
        }
    }
    
    /**
     * resizeAndRehash().
     */
    @SuppressWarnings("unchecked")
    private void resizeAndRehash() {
        Node[] newMap = (Node[]) new MyJHUHashMap.Node[maxCapacity * 2];

        for (int x = 0; x < maxCapacity; x++) {
            if (this.map[x] != null) {
                Node tempNode = this.map[x];
                int twice = maxCapacity * 2;
                int hash = Math.abs((tempNode.key.hashCode() % (twice)));
                
                while (newMap[hash] != null) {
                    hash = ((hash + 1) % (maxCapacity * 2));
                }
                
                newMap[hash] = tempNode;
            }
        }
        maxCapacity = maxCapacity * 2;
        this.map = newMap;
    }
    
    @Override
    public V get(Object k) {
        if (this.containsKey(k)) {
            int hash = Math.abs((k.hashCode() % (maxCapacity)));
            
            while (this.map[hash] == null || !this.map[hash].key.equals(k)) {
                hash = ((hash + 1) % (maxCapacity));
            }
            
            return this.map[hash].value;
        } else {
            return null;
        }
    }

    @Override
    public boolean containsKey(Object k) {
        int firstHash = Math.abs((k.hashCode() % (maxCapacity)));
        int hash = firstHash;
        
        while (this.map[hash] == null || !this.map[hash].key.equals(k)) {
            hash = ((hash + 1) % (maxCapacity));
            if (hash == firstHash) {
                return false;
            }
        }
        return true;
    }

    @Override
    public V remove(Object k) {
        if (this.containsKey(k)) {
            int hash = Math.abs((k.hashCode() % (maxCapacity)));
            
            while (this.map[hash] == null || !this.map[hash].key.equals(k)) {
                hash = ((hash + 1) % (maxCapacity));
            }
            
            V tempValue = this.map[hash].value;
            this.map[hash] = null;
            this.nodeCount--;
            return tempValue;
        } else {
            return null;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void clear() {
        this.nodeCount = 0;
        this.map = (Node[]) new MyJHUHashMap.Node[maxCapacity];
    }

    @Override
    public int size() {
        return this.nodeCount;
    }

    @Override
    public Set<K> keySet() {
        Set<K> keys = new HashSet<K>();

        for (int x = 0; x < maxCapacity; x++) {
            if (this.map[x] != null) {
                keys.add(this.map[x].key);
            }
        }
        
        return keys;
    }

    @Override
    public Collection<V> values() {
        Collection<V> vals = new ArrayList<V>();
        
        for (int x = 0; x < maxCapacity; x++) {
            if (this.map[x] != null) {
                vals.add(this.map[x].value);
            }
        }
        
        return vals;
    }
}