Strategy Used: I used a linked hashmap structure. This combined data structure allows me to store data in constant time while maintaining order. The keys get hashed to various locations in an array, but I also maintain a doubly linked list so that I can traverse the keys in a certain order.

Worst-Case Running Time For Processing A Single URL: O(1) - The URL is "instantly" hashed to a position in the array and added to the beginning of the doubly linked list chain (which is also constant time). Every once in a while the URL may have to increase its position in the doubly linked list chain because it is accessed more frequently (amortized to constant time).

Worst-Case Running Time For Outputting The K Most Frequently-Accessed Domains: O(k) - Only have to traverse through the doubly linked list (running time depends on k).

The only possible way to improve my running time is descreasing constants. There may be ways to make comparisons, hashcodes, etc. more efficiently, however, these improvements will only decrease running time very slightly.