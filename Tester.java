public class Tester {
    public static void main(String[] args) {
        MyJHUHashMap<Integer,Integer> m = new MyJHUHashMap<Integer,Integer>();
        m.put(10, 1);
        m.put(9, 1);
        m.put(10, 3);
        m.put(8, 3);
        m.put(7, 3);
        m.put(6, 3);
        m.put(5, 3);
        m.put(4, 3);
        m.put(90, 3);
        m.put(88, 3);
        m.put(77, 3);
        m.put(66, 3);
        m.put(55, 3);
        m.put(45, 3);
        m.put(33, 3);
        System.out.println(m.values());
    }
}